*&---------------------------------------------------------------------*
*& Report CHARM_GCTS_PS_KT_PRD_FEATURE
*&---------------------------------------------------------------------*

REPORT charm_gcts_ps_kt_prd_feature.

DATA lv_production_version TYPE p VALUE '1.0'.

DATA lv_feature_version TYPE p VALUE '2.0'.

DATA lv_emergency_version TYPE p VALUE '1.5'.
